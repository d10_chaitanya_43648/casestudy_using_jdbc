package dao;

import java.io.Closeable;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import pojo.User;
import test.Librarian;
import test.Member;
import test.Owner;
import utils.DBUtils;

public class UserDao implements Closeable {
	static Scanner sc = new Scanner(System.in);
	private static String owner = "owner";
	private static String librarian = "librarian";
	private static String member = "member";

	private Connection connection;
	private Statement statement;

	public UserDao() throws Exception {
		this.connection = DBUtils.getConnection();
		this.statement = this.connection.createStatement();
	}

	@Override
	public void close() throws IOException {
		try {
			connection.close();
			statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public void sighUp() throws SQLException {
		User user = UserDao.acceptUser();
		statement.executeUpdate("INSERT INTO user (name, email, phone, password, roleType) VALUES('" + user.getName() + "','" + user.getEmail() + "','"
				+ user.getPhone() + "','" + user.getPassword() + "','" + user.getRoleType() + "')");
		System.out.println("Sign Up successfully...Please Sign In Again!!!");
	}

	private static User acceptUser() {
		User user = new User();
		System.out.print("Enter Name : ");
		user.setName(sc.nextLine());
		System.out.print("Enter Email : ");
		user.setEmail(sc.nextLine());
		System.out.print("Enter Phone : ");
		user.setPhone(sc.nextLine());
		System.out.print("Enter Password : ");
		user.setPassword(sc.nextLine());
		user.setRoleType(UserDao.member);
		if (user.getEmail().equalsIgnoreCase("nikhilmahajan293@gmail.com"))
			user.setRoleType(UserDao.owner);
		return user;
	}

	public void signIn(String mail, String pw) throws SQLException {
		ResultSet rs = statement.executeQuery("SELECT * FROM user WHERE email = '"+mail+"' && password = '"+pw+"'");
		if(rs.next()) {
			if(rs.getString(3) != null) {
				if(rs.getString(6).equalsIgnoreCase(UserDao.librarian))
					Librarian.librarainArea();
				if(rs.getString(6).equalsIgnoreCase(UserDao.owner))
					Owner.ownerArea();
				if(rs.getString(6).equalsIgnoreCase(UserDao.member))
					Member.memberArea();	
			}
					
		}
	}

	public void sighUpLib() throws Exception {
		User user = UserDao.acceptUser();
		user.setRoleType(UserDao.librarian);
		statement.executeUpdate("INSERT INTO user (name, email, phone, password, roleType) VALUES('" + user.getName() + "','" + user.getEmail() + "','" + user.getPhone() + "','" + user.getPassword() + "','" + user.getRoleType() + "')");
		System.out.println("Sign Up successfully...Please Sign In Again!!!");
		
	}

	public void updateprofile(String nm, String ph, String mail) throws SQLException {
		statement.executeUpdate("UPDATE user SET name = '"+nm+"' WHERE email = '"+mail+"'");
		statement.executeUpdate("UPDATE user SET phone = '"+ph+"' WHERE email = '"+mail+"'");
		System.out.println("Profile Updated Successfully!!!");
	}

	public void updatepassword(String mail, String pw) throws Exception {
		statement.executeUpdate("UPDATE user SET password = '"+pw+"' WHERE email = '"+mail+"'");
		System.out.println("Password changed Successfully!!!");
	}

	public List<User> printMember() throws Exception {
		List<User> lt = new ArrayList<User>();
		ResultSet rs = statement.executeQuery("SELECT * FROM user WHERE roleType = '"+UserDao.member+"'");
		while(rs.next()) {
			User u = new User(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6));
			lt.add(u);
		}
		return lt;
	}

	
	
}
