package dao;

import java.io.Closeable;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import pojo.Book;
import utils.DBUtils;


public class BookDao implements Closeable{
	private Connection connection;
	private Statement statement;

	public BookDao() throws Exception {
		this.connection = DBUtils.getConnection();
		this.statement = this.connection.createStatement();
	}

	
	@Override
	public void close() throws IOException {
		try {
			connection.close();
			statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}


	public int addBook(Book book) throws Exception {
		return statement.executeUpdate("INSERT INTO book (name, author, subject, price, isbn) VALUES('"+book.getName()+"','"+book.getAuthor()+"','"+book.getSubject()+"',"+book.getPrice()+",'"+book.getIsbn()+"')");
	}

	public Book findBook(String nm) throws Exception {
		ResultSet rs = statement.executeQuery("SELECT * FROM book WHERE name = '"+nm+"'");
		if(rs.next()) {
			Book b = new Book(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getDouble(5), rs.getString(6));
			return b;
		}
		return null;
	}


	public boolean findBook(int i) throws Exception {
		return statement.executeQuery("SELECT * FROM book WHERE id = "+i+"") != null;
	}


	public void editBook(Book b) throws Exception {
		statement.executeUpdate("UPDATE book SET name = '"+b.getName()+"' WHERE id = "+b.getId()+"");
		statement.executeUpdate("UPDATE book SET author = '"+b.getAuthor()+"' WHERE id = "+b.getId()+"");
		statement.executeUpdate("UPDATE book SET subject = '"+b.getSubject()+"' WHERE id = "+b.getId()+"");
		statement.executeUpdate("UPDATE book SET price = "+b.getPrice()+" WHERE id = "+b.getId()+"");
		statement.executeUpdate("UPDATE book SET isbn = '"+b.getIsbn()+"' WHERE id = "+b.getId()+"");
		System.out.println("!!!Book Updated!!!");
	}


	public List<Book> printBook() throws Exception {
		List<Book> list = new ArrayList<Book>();
		ResultSet rs = statement.executeQuery("SELECT * FROM BOOK");
		while(rs.next()) {
			Book bk = new Book(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getDouble(5), rs.getString(6));
			list.add(bk);
		}
		return list;
	}


}
