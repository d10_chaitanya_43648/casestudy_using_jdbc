package dao;

import java.io.Closeable;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import pojo.IssueRecord;
import pojo.Payment;
import test.Librarian;
import utils.DBUtils;


public class IssueRecordDao implements Closeable{
	private Connection connection;
	private Statement statement;

	public IssueRecordDao() throws Exception {
		this.connection = DBUtils.getConnection();
		this.statement = this.connection.createStatement();
	}

	
	@Override
	public void close() throws IOException {
		try {
			connection.close();
			statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}


	public int addIssueCopy(IssueRecord ir) throws Exception {
		int year = ir.getIssueDate().getYear();
		int month = ir.getIssueDate().getMonthValue();
		int day = ir.getIssueDate().getDayOfMonth();
		String str =  year + "-"+month+"-"+day;
		
		year = ir.getReturnDueDate().getYear();
		month = ir.getReturnDueDate().getMonthValue();
		day = ir.getReturnDueDate().getDayOfMonth();
		String str1 = year + "-"+month+"-"+day;
		return statement.executeUpdate("INSERT INTO issueRecord (bookCopyId, memberId, issueDate, returnDuedate) VALUES ("+ir.getBookCopyId()+" , "
		+ir.getMemberId()+", '"+str+"','"+str1+"')");
	}


	public List<IssueRecord> displayIssuedMember(int i) throws Exception {
		List<IssueRecord> list = new ArrayList<IssueRecord>();
		ResultSet rs = statement.executeQuery("SELECT * FROM issueRecord WHERE memberID = "+i+"");
		while(rs.next()) {
			IssueRecord ir = new IssueRecord(rs.getInt(1), rs.getInt(2), rs.getInt(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getDouble(7));
			list.add(ir);
		}
		return list;
	}


	public void bookReturn(int m, int i) throws Exception {
		LocalDate dt = LocalDate.now();
		String str = dt.getYear()+"-"+dt.getMonthValue()+"-"+dt.getDayOfMonth();
		ResultSet rs = statement.executeQuery("SELECT DATEDIFF('"+str+"', returnDueDate) FROM issueRecord WHERE id = "+i+"");
		int diff = 0;
		int amt = 0;
		if(rs.next()) {
			diff = rs.getInt(1);
		}
		if(diff > 0) {
			amt = diff * 5;
			statement.executeUpdate("UPDATE issueRecord SET fineAmount = "+amt+" WHERE id = "+i+"");
			PaymentDao dao3 = new PaymentDao();
			Payment pay = new Payment();
			pay.setMemberId(m);
			pay.setType("Fine");
			pay.setAmount(amt);
			pay.setTxDate(LocalDate.now());
			dao3.addPayment(pay);
		}
		
		
	}
	
}
