package dao;

import java.io.Closeable;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import pojo.BookCopy;
import utils.DBUtils;

public class BookCopyDao implements Closeable{
	private Connection connection;
	private Statement statement;

	public BookCopyDao() throws Exception {
		this.connection = DBUtils.getConnection();
		this.statement = this.connection.createStatement();
	}

	
	@Override
	public void close() throws IOException {
		try {
			connection.close();
			statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}


	public int addBookCopy(BookCopy bc) throws Exception {
		return statement.executeUpdate("INSERT INTO bookCopy(bookId, rack, status) VALUES("+bc.getBookId()+" , "+bc.getRack()+" , '"+bc.getBookCopyStatus()+"')");
	}


	public int changeRack(int rc, int BCopyId) throws Exception {
		return statement.executeUpdate("UPDATE bookCopy SET rack = "+rc+" WHERE id = "+BCopyId+"");
	}


	public List<BookCopy> checkAvailability(int bId) throws Exception {
		List<BookCopy> bc = new ArrayList<BookCopy>();
		ResultSet rs = statement.executeQuery("SELECT * FROM bookCopy WHERE bookId = "+bId+"");
		while(rs.next()) {
			BookCopy b = new BookCopy(rs.getInt(1), rs.getInt(2), rs.getInt(3), rs.getString(4));
			bc.add(b);
		}
		return bc;
	}


	public void changeStatus(int bookCopyId) throws Exception {
		statement.executeUpdate("UPDATE bookCopy SET status = 'Issued' WHERE id = "+bookCopyId+"");
	}


	public void changeStatus1(int bookCopyId) throws Exception {
		statement.executeUpdate("UPDATE bookCopy SET status = 'Available' WHERE id = "+bookCopyId+"");		
	}
}
