package dao;

import java.io.Closeable;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import pojo.Payment;
import utils.DBUtils;


public class PaymentDao implements Closeable{
	private Connection connection;
	private Statement statement;

	public PaymentDao() throws Exception {
		this.connection = DBUtils.getConnection();
		this.statement = this.connection.createStatement();
	}

	
	@Override
	public void close() throws IOException {
		try {
			connection.close();
			statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public void addPayment1(Payment p) throws Exception {
		int year = p.getTxDate().getYear();
		int month = p.getTxDate().getMonthValue();
		int day = p.getTxDate().getDayOfMonth();
		String str =  year + "-"+month+"-"+day;
		year = p.getNextPayDueDate().getYear();
		month = p.getNextPayDueDate().getMonthValue();
		day = p.getNextPayDueDate().getDayOfMonth();
		String str1 =  year + "-"+month+"-"+day;
		statement.executeUpdate("INSERT INTO payment (memberId, type, amount, txDate, nextPayDueDate) VALUES("+p.getMemberId()+
				", '"+p.getType()+"', "+p.getAmount()+", '"+str+"', '"+str1+"')");
		
	}

	public void addPayment(Payment p) throws Exception {
		int year = p.getTxDate().getYear();
		int month = p.getTxDate().getMonthValue();
		int day = p.getTxDate().getDayOfMonth();
		String str =  year + "-"+month+"-"+day;
		statement.executeUpdate("INSERT INTO payment (memberId, type, amount, txDate) VALUES("+p.getMemberId()+
				", '"+p.getType()+"', "+p.getAmount()+", '"+str+"')");
		
	}


	public boolean checkPaidUser(int m, LocalDate issueDate) throws Exception {
		int year = issueDate.getYear();
		int month = issueDate.getMonthValue();
		int day = issueDate.getDayOfMonth();
		String str =  year + "-"+month+"-"+day;
		ResultSet diff = statement.executeQuery("SELECT DATEDIFF('"+str+"', nextPayDueDate) FROM payment WHERE memberId = "+m+" && !ISNULL(nextPayDueDate)");
		if(diff.next())
		{
			if(diff.getInt(1) < 0)
			return true;
			return false;
		}
		return false;
	}


	public List<Payment> paymentHistory() throws Exception {
		List<Payment> p = new ArrayList<Payment>();
		ResultSet rs = statement.executeQuery("SELECT * FROM payment");
		while(rs.next()) {
			Payment pay = new Payment(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getDouble(4));
			p.add(pay);
		}
		return p;
	}


	public void feesFineReport() throws Exception {
		ResultSet rs = statement.executeQuery("SELECT SUM(amount) FROM payment WHERE type = 'Fees' GROUP BY 'Fees' , amount");
		double amt = 0;
		while(rs.next()) {
			amt = amt + rs.getDouble(1);
		}
		System.out.println("Total Fees : "+amt);
		amt = 0;
		rs = statement.executeQuery("SELECT SUM(amount) FROM payment WHERE type = 'Fine' GROUP BY 'Fine' , amount");
		while(rs.next()) {
			amt = amt + rs.getDouble(1);
		}
		System.out.println("Total Fine : "+amt);
	}
}
