package pojo;

import java.time.LocalDate;

public class IssueRecord {
	private int id, bookCopyId, memberId;
	private LocalDate issueDate, returnDueDate, returnDate;
	private double fineAmount;
	private int bookReturnDuration = 7;
	public IssueRecord() {
		this.issueDate = LocalDate.now();
		this.returnDueDate = LocalDate.now().plusDays(bookReturnDuration);
		this.fineAmount = 0.0;
	}
	public IssueRecord(int bookCopyId, int memberId, double fineAmount) {
		super();
		this.bookCopyId = bookCopyId;
		this.memberId = memberId;
		this.issueDate = LocalDate.now();
		this.returnDueDate = LocalDate.now().plusDays(bookReturnDuration);
		this.fineAmount = fineAmount;
	}
	public IssueRecord(int id, int bookCopyId, int memberId, LocalDate issueDate, LocalDate returnDueDate,
			LocalDate returnDate, double fineAmount) {
		super();
		this.id = id;
		this.bookCopyId = bookCopyId;
		this.memberId = memberId;
		this.issueDate = issueDate;
		this.returnDueDate = returnDueDate;
		this.returnDate = returnDate;
		this.fineAmount = fineAmount;
	}
	public IssueRecord(int id, int bookCopyId, int memberId, String issueDate, String returnDueDate,
			String returnDate, double fineAmount) {
		super();
		this.id = id;
		this.bookCopyId = bookCopyId;
		this.memberId = memberId;
		String[] w1 = issueDate.split("-");
		String[] w2 = returnDueDate.split("-");
		
		
		this.issueDate = LocalDate.of(Integer.parseInt(w1[0]),Integer.parseInt( w1[1]), Integer.parseInt(w1[2]));
		this.returnDueDate = LocalDate.of(Integer.parseInt(w2[0]), Integer.parseInt(w2[1]), Integer.parseInt(w2[2]));
		this.returnDate = null;
		this.fineAmount = fineAmount;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getBookCopyId() {
		return bookCopyId;
	}
	public void setBookCopyId(int bookCopyId) {
		this.bookCopyId = bookCopyId;
	}
	public int getMemberId() {
		return memberId;
	}
	public void setMemberId(int memberId) {
		this.memberId = memberId;
	}
	public LocalDate getIssueDate() {
		return issueDate;
	}
	public void setIssueDate(LocalDate issueDate) {
		this.issueDate = issueDate;
	}
	public LocalDate getReturnDueDate() {
		return returnDueDate;
	}
	public void setReturnDueDate(LocalDate returnDueDate) {
		this.returnDueDate = returnDueDate;
	}
	public LocalDate getReturnDate() {
		return returnDate;
	}
	public void setReturnDate(LocalDate returnDate) {
		this.returnDate = returnDate;
	}
	public double getFineAmount() {
		return fineAmount;
	}
	public void setFineAmount(double fineAmount) {
		this.fineAmount = fineAmount;
	}
	public int getBookReturnDuration() {
		return bookReturnDuration;
	}
	public void setBookReturnDuration(int bookReturnDuration) {
		this.bookReturnDuration = bookReturnDuration;
	}
	public String toString1() {
		return String.format("%d	%d	  %d	%d-%d-%d 	%d-%d-%d	%.2f", this.id, this.bookCopyId, this.memberId, 
				this.issueDate.getDayOfMonth(), this.issueDate.getMonthValue(), this.issueDate.getYear(), 
				this.returnDueDate.getDayOfMonth(), this.returnDueDate.getMonthValue(), this.returnDueDate.getYear(), this.fineAmount);
	}
	public String toString() {
		return String.format("%d	%d	  %d	%d-%d-%d 	%d-%d-%d	%d-%d-%d	%.2f", this.id, this.bookCopyId, this.memberId, 
				this.issueDate.getDayOfMonth(), this.issueDate.getMonthValue(), this.issueDate.getYear(), 
				this.returnDueDate.getDayOfMonth(), this.returnDueDate.getMonthValue(), this.returnDueDate.getYear(),
				this.returnDate.getDayOfMonth(), this.returnDate.getMonthValue(), this.returnDate.getYear(), this.fineAmount);
	}
	
}
