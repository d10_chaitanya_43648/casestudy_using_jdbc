package pojo;

import java.time.LocalDate;

public class Payment {
	private int id;
	private int memberId;
	private String type;
	private double amount;
	private LocalDate txDate;
	private LocalDate nextPayDueDate;
	public Payment() {
	}
	public Payment(int id, int memberId, String type, double amount, LocalDate txDate, LocalDate nextPayDueDate) {
		super();
		this.id = id;
		this.memberId = memberId;
		this.type = type;
		this.amount = amount;
		this.txDate = txDate;
		this.nextPayDueDate = nextPayDueDate;
	}
	
	public Payment(int id, int memberId, String type, double amount) {
		super();
		this.id = id;
		this.memberId = memberId;
		this.type = type;
		this.amount = amount;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getMemberId() {
		return memberId;
	}
	public void setMemberId(int memberId) {
		this.memberId = memberId;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public LocalDate getTxDate() {
		return txDate;
	}
	public void setTxDate(LocalDate txDate) {
		this.txDate = txDate;
	}
	public LocalDate getNextPayDueDate() {
		return nextPayDueDate;
	}
	public void setNextPayDueDate(LocalDate nextPayDueDate) {
		this.nextPayDueDate = nextPayDueDate;
	}
	public String toString1() {
		return String.format("%d	%d	%-5s	%.2f", this.id, this.memberId, this.type, this.amount);
	}
	public String toString() {
		return String.format("%d	%d	%-5s	%.2f", this.id, this.memberId, this.type, this.amount, 
				this.txDate.getDayOfMonth(), this.txDate.getMonthValue(), this.txDate.getYear(), 
				this.nextPayDueDate.getDayOfMonth(), this.nextPayDueDate.getMonthValue(), this.nextPayDueDate.getYear());
	}
}
