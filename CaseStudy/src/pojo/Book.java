package pojo;

public class Book {
	private int id;
	private String name, author, subject;
	private double price;
	private String isbn;
	public Book() {
	}
	public Book(int id, String name, String author, String subject, double price, String isbn) {
		this.id = id;
		this.name = name;
		this.author = author;
		this.subject = subject;
		this.price = price;
		this.isbn = isbn;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public String getIsbn() {
		return isbn;
	}
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	public String toString() {
		return String.format("%d	%-20s%-20s%-15s%.2f		%-20s", this.id, this.name, this.author, this.subject, this.price, this.isbn);
	}
}
