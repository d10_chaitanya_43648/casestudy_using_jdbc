package pojo;

public class BookCopy {
	private int id;
	private int bookId;
	private int rack;
	private String bookCopyStatus;
	public BookCopy() {
	}
	public BookCopy(int id, int bookId, int rack, String bookCopyStatus) {
		super();
		this.id = id;
		this.bookId = bookId;
		this.rack = rack;
		this.bookCopyStatus = bookCopyStatus;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getBookId() {
		return bookId;
	}
	public void setBookId(int bookId) {
		this.bookId = bookId;
	}
	public int getRack() {
		return rack;
	}
	public void setRack(int rack) {
		this.rack = rack;
	}
	public String getBookCopyStatus() {
		return bookCopyStatus;
	}
	public void setBookCopyStatus(String bookCopyStatus) {
		this.bookCopyStatus = bookCopyStatus;
	}
	public String toString() {
		return String.format("BookCopy Id: %d	BookId : %d		Rack : %d	Status : %s", this.id, this.bookId, this.rack, this.bookCopyStatus);
	}
}
