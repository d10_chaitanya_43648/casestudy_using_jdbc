package test;

import java.util.List;
import java.util.Scanner;

import dao.BookCopyDao;
import dao.BookDao;
import dao.PaymentDao;
import dao.UserDao;
import pojo.Book;
import pojo.BookCopy;
import pojo.User;

public class Owner {
	static Scanner sc = new Scanner(System.in);
	public static int ownermenu() {
		System.out.println("0. Sign Out");
		System.out.println("1. Appoint Librarian");
		System.out.println("2. Edit Profile");
		System.out.println("3. Change Password");
		System.out.println("4. Fees/Fine Report");
		System.out.println("5. Book Availability");
		System.out.println("6. Book Categories/Subjects");
		System.out.println("7. Display Member");
		System.out.print("Enter choice: ");
		return sc.nextInt();       
	}
	public static void ownerArea() {
		try(UserDao dao = new UserDao();
			BookDao dao1 = new BookDao();
			BookCopyDao dao2 = new BookCopyDao();
			PaymentDao dao3 = new PaymentDao();){
			String[] name = new String[1];
			String[] phone = new String[1];
			String[] email = new String[1];
			String[] password = new String[1];
			int[] bookId = new int[1];
		int choice;
		while((choice = Owner.ownermenu()) != 0) {
			switch(choice) {
			case 1:
				dao.sighUpLib();
				break;
			case 2:
				Owner.editProfile(name, phone, email);
				dao.updateprofile(name[0], phone[0], email[0]);
				break;
			case 3:
				Owner.editPassword(email, password);
				dao.updatepassword(email[0], password[0]);
				break;
			case 4://Fees/Fine Report
				dao3.feesFineReport();
				break;
			case 5:
				Owner.acceptBookId(bookId);
				List<BookCopy> list = dao2.checkAvailability(bookId[0]);
				Owner.printCheckAvailability(list);
				break;
			case 6:
				List<Book> b = dao1.printBook();
				Owner.printBook(b);
				break;
			case 7:
				List<User> user =  dao.printMember();
				Owner.printMember(user);
				break;
			}
		}
		}catch(Exception ex) {
			ex.printStackTrace();
		}
	}
	
	private static void printBook(List<Book> b) {
		if(b != null) {
			for (Book book : b) {
				System.out.println(book.toString());
			}
		}
		
	}
	private static void printMember(List<User> user) {
		if(user != null) {
			for (User u : user) {
				System.out.println(u.toString());
			}
		}
		
	}
	private static void printCheckAvailability(List<BookCopy> list) {
		if(list != null) {
			for (BookCopy bc : list) {
				System.out.println(bc.toString());
			}
		}
		
	}
	private static void acceptBookId(int[] bookId) {
		System.out.print("Enter Book ID : ");
		bookId[0] = sc.nextInt();
	}
	private static void editPassword( String[] email, String[] password) {
		sc.nextLine();
		System.out.print("Verify Email : ");
		email[0] = sc.nextLine();
		System.out.print("Enter New Password : ");
		password[0] = sc.nextLine();		
	}
	
	private static void editProfile(String[] name, String[] phone, String[] email) {
		sc.nextLine();
		System.out.print("Verify Email : ");
		email[0] = sc.nextLine();
		System.out.print("Enter New Name : ");
		name[0] = sc.nextLine();
		System.out.print("Enter New Phone : ");
		phone[0] = sc.nextLine();	
	}
}
