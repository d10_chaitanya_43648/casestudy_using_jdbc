package test;

import java.util.List;
import java.util.Scanner;

import dao.BookCopyDao;
import dao.BookDao;
import dao.UserDao;
import pojo.Book;
import pojo.BookCopy;

public class Member {
	static Scanner sc = new Scanner(System.in);
	public static int membermenu() {
		System.out.println("0. Sign Out");
		System.out.println("1. Find Book");
		System.out.println("2. Edit Profile");
		System.out.println("3. Change Password");
		System.out.println("4. Book Availability");
		System.out.print("Enter choice: ");
		return sc.nextInt();       
	}
	public static void memberArea() {
		try(UserDao dao = new UserDao();
			BookDao dao1 = new BookDao();
			BookCopyDao dao2 = new BookCopyDao();){
		int choice;
		String[] name = new String[1];
		String[] phone = new String[1];
		String[] email = new String[1];
		String[] password = new String[1];
		int[] bookId = new int[1];
		while((choice = Member.membermenu()) != 0) {
			switch(choice) {
			case 1://Find Book
				Member.acceptBookName(name);
				Book bk = dao1.findBook(name[0]);
				Member.printBook(bk);
				break;
			case 2:
				Member.editProfile(name, phone, email);
				dao.updateprofile(name[0], phone[0], email[0]);
				break;
			case 3:
				Member.editPassword(email, password);
				dao.updatepassword(email[0], password[0]);
				break;
			case 4:
				Member.acceptBookId(bookId);
				List<BookCopy> list = dao2.checkAvailability(bookId[0]);
				Member.printCheckAvailability(list);
				break;
			}
		}
		}catch(Exception ex) {
			ex.printStackTrace();
		}
	}
	
	private static void printBook(Book bk) {
		if(bk != null)
			System.out.println(bk.toString());
		else
			System.out.println("Book Not Found!!!");
	}
	private static void acceptBookName(String[] book) {
		sc.nextLine();
		System.out.print("Enter Book Name : ");
		book[0] = sc.nextLine();
	}
	private static void printCheckAvailability(List<BookCopy> list) {
		if(list != null) {
			for (BookCopy bc : list) {
				System.out.println(bc.toString());
			}
		}
		
	}
	private static void acceptBookId(int[] bookId) {
		System.out.print("Enter Book ID : ");
		bookId[0] = sc.nextInt();
	}
	private static void editPassword( String[] email, String[] password) {
		sc.nextLine();
		System.out.print("Verify Email : ");
		email[0] = sc.nextLine();
		System.out.print("Enter New Password : ");
		password[0] = sc.nextLine();		
	}
	
	private static void editProfile(String[] name, String[] phone, String[] email) {
		sc.nextLine();
		System.out.print("Verify Email : ");
		email[0] = sc.nextLine();
		System.out.print("Enter New Name : ");
		name[0] = sc.nextLine();
		System.out.print("Enter New Phone : ");
		phone[0] = sc.nextLine();	
	}
}
