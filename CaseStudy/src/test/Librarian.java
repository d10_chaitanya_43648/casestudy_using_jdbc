package test;

import java.time.LocalDate;
import java.util.List;
import java.util.Scanner;

import dao.BookCopyDao;
import dao.BookDao;
import dao.IssueRecordDao;
import dao.PaymentDao;
import dao.UserDao;
import pojo.Book;
import pojo.BookCopy;
import pojo.IssueRecord;
import pojo.Payment;
import pojo.User;

public class Librarian {
	static Scanner sc = new Scanner(System.in);
	private static String status = "Available";

	public static int libmenu() {
		System.out.println("0. Sign Out");
		System.out.println("1. Add member");
		System.out.println("2. Edit Profile");
		System.out.println("3. Change Password");
		System.out.println("4. Add Book");
		System.out.println("5. Find Book");
		System.out.println("6. Edit Book");
		System.out.println("7. Check Availability");
		System.out.println("8. Add Copy");
		System.out.println("9. Change Rack");
		System.out.println("10. Issue Copy");
		System.out.println("11. Return Copy");
		System.out.println("12. Take Payment");
		System.out.println("13. Payment History");
		System.out.println("14. Display member");
		System.out.println("15. List of Subject");
		System.out.print("Enter choice: ");
		return sc.nextInt();
	}

	public static void librarainArea() {
		try(UserDao dao = new UserDao();
			BookDao dao1 = new BookDao();
			BookCopyDao dao2 = new BookCopyDao();
			PaymentDao dao3 = new PaymentDao();
			IssueRecordDao dao4 = new IssueRecordDao();){
		int choice;
		String[] name = new String[1];
		String[] phone = new String[1];
		String[] email = new String[1];
		String[] password = new String[1];
		String[] bookName = new String[1];
		int[] bookId = new int[1];
		int[] bookCopyId = new int[1];
		int[] rack = new int[1];
		int[] memberId = new int[1];
		int[] issueRecordId = new int[1];
		while((choice = Librarian.libmenu()) != 0) {
			switch(choice) {
			case 1:
				dao.sighUp();
				break;
			case 2:
				Librarian.editProfile(name, phone, email);
				dao.updateprofile(name[0], phone[0], email[0]);
				break;
			case 3:
				Librarian.editPassword(email, password);
				dao.updatepassword(email[0], password[0]);
				break;
			case 4:
				Book book = Librarian.addBook();
				int status = dao1.addBook(book);
				Librarian.printStatus(status);
				break;
			case 5:
				Librarian.acceptBookName(bookName);
				Book bk = dao1.findBook(bookName[0]);
				Librarian.printBook(bk);
				break;
			case 6:
				Librarian.acceptBookId(bookId);
				boolean sts = dao1.findBook(bookId[0]);
				if(sts) {
					Book b = Librarian.addBook();
					b.setId(bookId[0]);
					dao1.editBook(b);
				}else
					Librarian.bookNotFound();
				break;
			case 7://7. Check Availability
				Librarian.acceptBookId(bookId);
				List<BookCopy> list = dao2.checkAvailability(bookId[0]);
				Librarian.printCheckAvailability(list);
				break;
			case 8://8. Add Copy
				BookCopy bc = Librarian.acceptBookCopy();
				int status1 = dao2.addBookCopy(bc);
				Librarian.printBookCopyStatus(status1);
				break;
			case 9://9. Change Rack
				Librarian.acceptRack(bookCopyId, rack);
				int rackStatus = dao2.changeRack(rack[0], bookCopyId[0]);
				Librarian.printRackStatus(rackStatus);
				break;
			case 10://Issue Copy
				IssueRecord ir = Librarian.acceptInfo();
				if(Librarian.paidUser(ir)) {
					int st = dao4.addIssueCopy(ir);
					Librarian.printIssueCopyStatus(st);
					if(st != 0) {
						dao2.changeStatus(ir.getBookCopyId());
					}
				}
				break;
			case 11://Return Copy
				Librarian.acceptMemberId(memberId);
				List<IssueRecord> i =  dao4.displayIssuedMember(memberId[0]);
				Librarian.displayMember(i);
				Librarian.acceptissueRecordId(issueRecordId, bookCopyId);
				dao4.bookReturn(memberId[0], issueRecordId[0]);
				dao2.changeStatus1(bookCopyId[0]);
				break;
			case 12://Take Payment
				Payment payment =  Librarian.acceptPayment();
				dao3.addPayment1(payment);
				break;
			case 13://Payment History
				List<Payment> lt = dao3.paymentHistory();
				Librarian.printHistory(lt);
				break;
			case 14:
				List<User> user =  dao.printMember();
				Librarian.printMember(user);
				break;
			case 15:
				List<Book> b = dao1.printBook();
				Librarian.printBook(b);
				break;
			}
		}
		}catch(Exception ex) {
			ex.printStackTrace();
		}
	}
	
	private static void printHistory(List<Payment> lt) {
		if(lt != null) {
			for (Payment payment : lt) {
				System.out.println(payment.toString1());
			}
		}
		
	}

	private static void acceptissueRecordId(int[] issueRecordId, int[] bookCopyId) {
		System.out.print("Enter IssueRecordId : ");
		issueRecordId[0] = sc.nextInt();
		System.out.print("Enter bookCopyId : ");
		bookCopyId[0] = sc.nextInt();
	}

	private static void displayMember(List<IssueRecord> i) {
		if(i != null) {
			for (IssueRecord issueRecord : i) {
				System.out.println(issueRecord.toString1());
			}
		}
		
	}

	private static void acceptMemberId(int[] memberId) {
		System.out.print("Enter Member Id : ");
		memberId[0] = sc.nextInt();
	}

	private static void printIssueCopyStatus(int st) {
		if(st != 0)
			System.out.println("BookCopy Issued!!!");
		else
			System.out.println("Not Issued!!!");	
	}

	private static boolean paidUser(IssueRecord ir) throws Exception {
		PaymentDao dao = new PaymentDao();
		return dao.checkPaidUser(ir.getMemberId(), ir.getIssueDate());
	}

	private static IssueRecord acceptInfo() {
		IssueRecord ir = new IssueRecord();
		System.out.print("Enter BookCopy Id : ");
		ir.setBookCopyId(sc.nextInt());
		System.out.print("Enter Member Id : ");
		ir.setMemberId(sc.nextInt());
		ir.setIssueDate(LocalDate.now());
		ir.setReturnDueDate(LocalDate.now().plusDays(7));
		return ir;
	}

	private static Payment acceptPayment() {
		Payment pay = new Payment();
		System.out.print("Enter Mamber ID : ");
		pay.setMemberId(sc.nextInt());
		pay.setType("Fees");
		System.out.print("Enter Fees Amount : ");
		pay.setAmount(sc.nextDouble());
		pay.setTxDate(LocalDate.now());
		pay.setNextPayDueDate(LocalDate.now().plusDays(30));
		return pay;
	}

	private static void printBook(List<Book> b) {
		if(b != null) {
			for (Book book : b) {
				System.out.println(book.toString());
			}
		}
		
	}
	private static void printMember(List<User> user) {
		if(user != null) {
			for (User u : user) {
				System.out.println(u.toString());
			}
		}
		
	}
	private static void printCheckAvailability(List<BookCopy> list) {
		if(list != null) {
			for (BookCopy bc : list) {
				System.out.println(bc.toString());
			}
		}
		
	}

	private static void acceptRack(int[] bookCopyId, int[] rack) {
		System.out.print("Enter bookCopyId : ");
		bookCopyId[0] = sc.nextInt();
		System.out.print("Enter Rack : ");
		rack[0] = sc.nextInt();		
	}

	private static void printRackStatus(int rackStatus) {
		if(rackStatus != 0)
			System.out.println("Rack Changed successfully!!!");
		else
			System.out.println("Rack Not Changed!!!");		
	}

	private static void printBookCopyStatus(int status1) {
		if(status1 != 0)
			System.out.println("Book Copy Added successfully!!!");
		else
			System.out.println("Book Not Added!!!");		
	}

	private static BookCopy acceptBookCopy() {
		BookCopy bookCopy = new BookCopy();
		System.out.print("Enter BookID : ");
		bookCopy.setBookId(sc.nextInt());
		System.out.print("Enter Rack : ");
		bookCopy.setRack(sc.nextInt());
		bookCopy.setBookCopyStatus(Librarian.status );
		return bookCopy;
	}

	private static void bookNotFound() {
		System.out.println("!!!Book Not Found!!!");		
	}

	private static void acceptBookId(int[] bookId) {
		System.out.print("Enter Book ID : ");
		bookId[0] = sc.nextInt();
	}

	private static void printBook(Book bk) {
		if(bk != null)
			System.out.println(bk.toString());
		else
			System.out.println("Book Not Found!!!");
	}

	private static void acceptBookName(String[] book) {
		sc.nextLine();
		System.out.print("Enter Book Name : ");
		book[0] = sc.nextLine();
	}

	private static void printStatus(int status) {
		if(status != 0)
			System.out.println("Book Added successfully!!!");
		else
			System.out.println("Book Not Added!!!");
	}

	private static Book addBook() {
		Book bk = new Book();
		sc.nextLine();
		System.out.print("Enter Book Name : ");
		bk.setName(sc.nextLine());
		System.out.print("Enter Book Author : ");
		bk.setAuthor(sc.nextLine());
		System.out.print("Enter Book Subject : ");
		bk.setSubject(sc.nextLine());
		System.out.print("Enter Book Price : ");
		bk.setPrice(sc.nextDouble());
		sc.nextLine();
		System.out.print("Enter Book ISBN : ");
		bk.setIsbn(sc.nextLine());
		return bk;
	}

	private static void editPassword( String[] email, String[] password) {
		sc.nextLine();
		System.out.print("Verify Email : ");
		email[0] = sc.nextLine();
		System.out.print("Enter New Password : ");
		password[0] = sc.nextLine();		
	}
	
	private static void editProfile(String[] name, String[] phone, String[] email) {
		sc.nextLine();
		System.out.print("Verify Email : ");
		email[0] = sc.nextLine();
		System.out.print("Enter New Name : ");
		name[0] = sc.nextLine();
		System.out.print("Enter New Phone : ");
		phone[0] = sc.nextLine();	
	}
}
