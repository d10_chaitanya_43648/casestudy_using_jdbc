package test;

import java.util.Scanner;

import dao.UserDao;

public class Program {
	static Scanner sc = new Scanner(System.in);
	private static void signIn(String[] s) {
		sc.nextLine();
		System.out.print("Enter Email : ");
		s[0] = sc.nextLine();
		System.out.print("Enter Password : ");
		s[1] = sc.nextLine();
	}

	public static int menu() {
		System.out.println("0. Exit");
		System.out.println("1. Sign In");
		System.out.println("2. Sign Up");
		System.out.print("Enter choice : ");
		return sc.nextInt();
	}

	public static void main(String[] args) {
		try(UserDao dao = new UserDao();){
		int choice;
		String[] s = new String[2];
		while((choice = Program.menu()) != 0) {
			switch(choice) {
			case 1:
				Program.signIn(s);
				dao.signIn(s[0], s[1]);
				break;
			case 2:
				dao.sighUp();
				break;
			}
		}
		}catch(Exception ex) {
			ex.printStackTrace();
		}
	}
	
}
